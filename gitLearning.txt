git commit
git branch bugFix
git checkout bugFix
git merge bugFix  目標bugFix併過來		//生成一個新的commit並同時繼承master跟bugFix
git rebase master	併到目標master去		//把分支整個複製到master下面 然後原本的分支拋棄
git rebase bugFix


head一開始都指向branch
git checkout c4 讓head轉成指向commit
git checkout bugFix^ 讓head指向bugFix的parent commit
git checkout master~4
git checkout HEAD^2		在merge後會出現多個parent commit的狀況, 用"HEAD^2"代表第二個parent commit
git branch -f master HEAD~2 把branch master 移動到head往上數兩個  (如果沒有branch master就會新建一個放到head往上數兩個的commit)

git reset HEAD~1 這是local的reset 把目前head取消, head回到上一個
git revert HEAD 	這是remote的revert會同時把結果分享給大家, 效果是取消目前修改過的cmmit, 並把原本的commit連在下面

git cherry-pick c3 c4 c7 把c3 c4 c7的commit複製並貼到head下方								
git rebase -i HEAD~4	會出現UI 把head+前面3個依想要的順序排列或隱藏
git tag v0 c1					建立里程碑tag v0到c1上
git describe master			顯示距離master最近的tag跟距離幾個commit  (v0_2_gc2)代表master指向c2距離最近的tag v0兩個commit
git describe					顯示距離head最近的tag跟距離幾個commit
git clone						(只有一開始能用)創建一個新的備份到遠端, 本機端加上branch代表目前複製到遠端的部分 o/master = origin/master
git fetch						下載備份到本機, 本機的o/master會跟著遠端更新 但本機端的branch不會更新
git pull							下載備份到本機, o/master會更新, 且會把本機的branch跟新的branch o/master 合併merge在一起
git fakeTeamwork master 2	對遠端的branch master底下加入兩個commit
git push							把本機的commit上傳到遠端, 同時更新遠端最新的master, 在更新本機的remote branch到最新

git pull; git push;			//下載遠端+跟本機merge 最後再更新到遠端並同步本機的o/master
git pull --rebase; git push	//下載遠端+ 把本機head 合併到更新的o/master下方 最後再更新到遠端並同步本機的o/master  (遠端的圖比較好看)
git pull origin foo:side	//下載remote的foo到本機的side, 並且與HEAD合併(如果side不在,會新創一個在foo的位置)


git checkout -b foo o/master		//新建一個branch foo並track到remote的master(本機的master還有track到遠端的master),
													之後head在foo也可以pull跟push
git branch -u o/master foo			//把原本存在的foo track到remote的master, 之後head在foo也可以pull跟push

git push origin master		//檢查本機跟 origin remote上的master是否一樣, 不一樣就把本機update到遠端(HEAD不一定要在master上)
git push origin foo:master	//檢查本機的foo跟origin remote的master是否一樣, 
											不一樣就把本機foo update到remote端  (HEAD不需要在foo或master上) 
											如果master不在,會新創一個在remote的foo位置
git push origin :newOne		//刪除遠端newOne跟本機o/newOne

											
git fetch origin foo						//把remote端的foo更新到本機端的o/foo	(HEAD不用在foo上)
git fetch origin foo^:master		//把remote端的foo的parent更新到本機端的o/master 
													(如果本機沒有master, 會新創一個master branch在本機端的foo) HEAD不用在master上
git fetch origin :bar			//就是git branch bar													

													


level 章節 到目標章節； level 回到主目錄
undo上一步
reset回到一開始
show solution 看解答

//bug fix之後把debug跟printf的機制拿掉 然後把修改過後的code接在master下方
git rebase -i HEAD~3		//把debug跟printf拿掉
git checkout master
git rebase bugFix				//把master更新成bugFix

//要對之前newImage的commit做修改 用rebase

git rebase -i HEAD~2			//把caption跟newImage互換讓head指向newImage
git commit --amend				//對newImage做修改用指令git commit --amend
git rebase -i HEAD~2			//再把順序換回來
git checkout master				//head改指向master
git rebase caption					//讓master併到caption

//要對之前newImage的commit做修改 用cherry-pick

git checkout newImage			//把head先轉到要改的newImage
git commit --amend				//對newImage做修改
git checkout master				//把head轉到master
git cherry-pick c2' c3				//直接在master下方把修改過後的newImage c2'跟 要連接的commit c3 連起來

//tag是里程碑的意思 在tag上產生commit是無效的(會隱藏)

git tag v0 c1				//建tag v0到c1上
git tag v1 c2				//建tag v1到c2上
git checkout v1			//head移到v1指的commit 因為v1是tag里程碑, 所以head會跟v1分開


git describe master				顯示距離master最近的tag
v0_2_gc2							master最近的tag是v0共距離2個commit, master指的是c2
git describe side					顯示距離side最近的tag
v1_1_gc4							side最近的tag是v1共距離1個commit, master指的是c4
git describe bugFix			顯示距離bugFix最近的tag
v1_2_gc6							bugFix最近的tag是v1共距離2個commit, master指的是c6

要把所有branch合併之後再排序就是先rebase到每個branch的最外點, 再一次用rebase -i做排序

cherry-pick用在已知要連接的有那些commit			cherry-pick可以用在連接別的分支的parent
rebase -i用在不確定要連接的parent commit有哪些  rebase只能用在連接自己的parent

在master上 git commit 會出現實心的commit 
在o/master上 git commit會出現虛線的commit 請問實心跟虛線的commit差在哪??


git clone											//將本機的commit複製到遠端
git fakeTeamwork master 2				//在遠端的master下生成2個commit
git commit										//在本機端的head生1個commit
git pull												//把遠端commit下載並跟新o/master 最後再把head的branch跟o/master的branch合併

git fetch
git rebase o/master side1					//把side1併到o/master下面 並且head到side1
git rebase side1 side2							//side2併到side1下面, head 到side2
git rebase side2 side3							//side3併到side2下面, head指到side3
git rebase side3 master						//當在同一條commit上的時候, rebase會把master branch移到side3
git push												//上傳到remote

